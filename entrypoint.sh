#!/bin/sh

# if anything fails, return a failure to the log
set -e

envsubst </etc/nginx/default.conf.tpl >/etc/nginx/conf.d/default.conf
nginx -g 'daemon off;'
